'''
test file for model.py
'''

__author__ = 'ayush.abhinav@tcs.com'


import unittest
from unittest import TestCase

from csv_to_json.model import Item

class TestItem(TestCase):
    '''
    contains test case for  item class
    '''

    def test_create_item(self):
        '''
        test to check of item is created successfully
        :return: None
        '''
        id = 1
        name = "test_item"
        link = "reference_link"

        item = Item(name, id, link)
        self.assertIsInstance(item, Item)
        self.assertEqual(item.id, id, "id is incorrect")
        self.assertEqual(item.label, name, "name is incorrect")
        self.assertEqual(item.link, link, "link is incorrect")
        self.assertEqual(len(item.children), 0, "item should not have child as \
                                            it is not added yet")


    def test_add_child(self):
        '''
        test case to check if child item is added successfully
        :return: None
        '''
        id = 1
        name = "parent_item"
        link = "parent_link"
        parent_item = Item(name, id, link)

        id = 2
        name = "child_item"
        link = "child_link"
        child_item = Item(name, id, link)

        parent_item.add_child(child_item)
        self.assertEqual(len(parent_item.children), 1, "One child has been added")


        id = 3
        name = "child_item2"
        link = "child_link2"
        child_item2 = Item(name, id, link)

        parent_item.add_child(child_item2)
        self.assertEqual(len(parent_item.children), 2, "Two child items have been added")



        test_child = parent_item.children[0]
        test_child2 = parent_item.children[1]
        self.assertEqual(test_child.id, child_item.id, "child item id is incorrect")
        self.assertEqual(test_child2.id, child_item2.id, "child item is incorrect")




    def test_equal(self):
        '''
        test case for __eq__ on Item class
        :return: None
        '''
        id = 1
        name = "test_item"
        link = "reference_link"

        item = Item(name, id, link)
        other_item = Item(name, id, link)

        self.assertTrue(item == other_item, "issue with __eq__. should return True")

        id = 2
        name = "test_item"
        link = "reference_link"
        other_item = Item(name, id, link)

        self.assertFalse(item == other_item, "issue with __eq__. should return False")




if __name__ == '__main__':
    unittest.main()
