'''
testfile for CSVToJson
'''

__author__ = 'ayush.abhinav@tcs.com'

import os
import types
import unittest
from unittest import TestCase

from csv_to_json.csv_to_json import CSVToJSON
from csv_to_json import model


class TestCSVToJSON(TestCase):
    '''
    Test class fro CSVToJson
    '''

    def setUp(self):
        root_folder = os.path.dirname(__file__)
        self.input_file = os.path.join(root_folder, 'test_csv_file.csv')
        self.output_file = os.path.join(root_folder, 'test_out_file.json')
        self.check_files = os.path.join(root_folder, 'check_file.json')


    def tearDown(self):
        if os.path.exists(self.output_file):
            os.remove(self.output_file)


    def test_get_line_from_csv_file(self):
        '''
        test for generator to csv file
        :return:
        '''
        ctj = CSVToJSON(self.input_file, self.output_file)
        infile = ctj.get_line_from_csv_file()
        self.assertIsInstance(infile, types.GeneratorType, "Generator Object is expected")



    def test_create_object(self):
        '''
        test for object creation function
        :return:
        '''
        ctj = CSVToJSON(self.input_file, self.output_file)
        root_item = ctj.create_object()
        self.assertIsInstance(root_item, model.Item, "Item object is expected")

        result_item = model.Item("THE BEST", "178974",
                                 "https://groceries.morrisons.com/browse/178974")

        self.assertEqual(root_item.label, result_item.label, "Item should be same")





    def test_get_json(self):
        '''
        Test for get json function
        :return:
        '''
        ctj = CSVToJSON(self.input_file, self.output_file)
        ctj.create_object()
        ctj.get_json()

        json_text = None
        check_text = None
        with open(self.output_file, 'r') as out_file_fp:
            json_text = out_file_fp.read()

        with open(self.check_files, 'r') as check_file_fp:
            check_text = check_file_fp.read()

        self.assertEqual(json_text, check_text, "Output json date is incorrect")


if __name__ == "__main__":
    unittest.main()
