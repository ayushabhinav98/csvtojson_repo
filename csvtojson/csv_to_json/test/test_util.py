'''
test cases for the util functions
'''

__author__ = 'ayush.abhinav@tcs.com'


import unittest
from unittest import TestCase

from csv_to_json.util import get_tokens


class TestUtilFunctions(TestCase):
    '''
    Class containing the test cases for functions in util module
    '''

    def test_get_tokens(self):
        '''
        test for util function get_tokens
        :return:None
        '''
        input_line = "This, is, my ,input, line"
        tokens = get_tokens(input_line)
        self.assertEqual(len(tokens), 5, "Wrong token count")
        self.assertEqual(tokens[1], 'is', "Wrong token position")


        input_line = 'This, is, my, "input, line"'
        tokens = get_tokens(input_line)
        self.assertEqual(len(tokens), 4, "Wrong parsing of comma in one word")


if __name__ == "__main__":
    unittest.main()
