'''
custom exception for csv_to_json is listed here
'''

__author__ = 'ayush.abhinav@tcs.com'


class CSVDataException(Exception):
    '''
    exception due to incorrect data
    '''


class CSVDataParsingException(Exception):
    '''
    exception due to issue in parsing of the line
    '''

    def __init__(self, line):
        self.message = "Error in parsing line: {}".format(line)
        super().__init__(self.message)
        