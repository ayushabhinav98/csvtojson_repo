'''
contains utility functions
'''

__author__ = 'ayush.abhinav@tcs.com'

from csv_to_json.exception import CSVDataParsingException, CSVDataException


def get_tokens(line):
    '''
    parse the csv line
    :param line: csv input line
    :return: list of seperated values
    '''
    try:
        def _get_end_pos(start_pos):
            for idx, token in enumerate(tokens[start_pos :]):
                if token.endswith('"'):
                    return start_pos + idx
            return None


        tokens = [i.strip() for i in line.split(',') if i.strip() != '']
        split_token_pos = []
        for idx, token in enumerate(tokens):
            if str(token).startswith('"') and str(token).endswith('"'):
                pass
            elif str(token).startswith('"'):
                start_pos = idx
                end_pos = _get_end_pos(start_pos)
                if end_pos is None:
                    raise CSVDataException("extra comma in line. closing quotes cannot be found")
                split_token_pos.insert(0, (start_pos, end_pos))


        for start_pos, end_pos in split_token_pos:
            tokens[start_pos] = ' '.join(tokens[start_pos: end_pos + 1])
            del tokens[start_pos + 1 : end_pos + 1]


        return tokens
    except Exception:
        raise CSVDataParsingException(line=line)
