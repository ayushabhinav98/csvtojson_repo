'''
contains CSVToJSON class
'''
import sys
import json
import logging

from csv_to_json.model import Item, ItemEncoder
from csv_to_json.util import get_tokens


class CSVToJSON:
    '''
    Driver class for converting CSV to JSON
    '''
    def __init__(self, input_file, output_file):
        self.input_file = input_file
        self.output_file = output_file
        self.root = None
        self.logger = logging.getLogger('CSVToJSON')


    def get_line_from_csv_file(self):
        '''
        generator function for reading the csv file
        :return: generator to csvfile
        '''
        try:
            with open(self.input_file, 'r') as csv_file:
                for line in csv_file:
                    yield line
        except FileNotFoundError:
            self.logger.error('file: %s could not be found', self.input_file)



    def create_object(self, has_header=True):
        '''
        create object from the content of csv file
        :param has_header: bool, if csv file contains header, default to True
        :return: RootItem Object
        '''
        try:
            item_list = list()

            for line in self.get_line_from_csv_file():
                line = line.strip()
                if has_header:
                    has_header = False
                    continue
                if line.strip() == '':
                    continue

                tokens = get_tokens(line)
                if len(tokens) == 0:
                    continue

                self.logger.debug(tokens)

                if self.root is None:
                    self.root = Item(tokens[-3], tokens[-2], tokens[-1])
                    item_list.append(self.root)
                else:
                    parent_item = Item(tokens[-6], tokens[-5], tokens[-4])
                    item = Item(tokens[-3], tokens[-2], tokens[-1])
                    parent_idx = -1
                    parent = None
                    while parent_idx > -len(item_list) -1:
                        if item_list[parent_idx] == parent_item:
                            parent = item_list[parent_idx]
                            break
                        parent_idx = parent_idx - 1

                    parent.add_child(item)
                    if parent_idx != -1:
                        del item_list[parent_idx + 1:]
                    item_list.append(item)
            return self.root
        except Exception as ex:
            self.logger.error("error occured in csv to object creation: %s", ex)


    def print_item(self, item=None, spaces=' '):
        '''
        method to print the object tree on stdout
        :param item: item to start traversal from
        :param spaces: create indentation. defaults to ' '. other values may be #,-
        :return:
        '''
        if item is None:
            item = self.root
        print('{} {}'.format(spaces, item))
        if hasattr(item, 'children'):
            for child in item.children:
                self.print_item(child, spaces + ' ' * 2)


    def get_json(self, encoder=ItemEncoder, indent=4):
        '''
        print the object in json format
        :param encoder: encoder to use. defaults to RootItemEncoder
        :param indent: for pretty printing. defaults to 4
        :return: None
        '''
        try:
            if self.output_file is sys.stdout:
                json.dump([self.root], self.output_file, cls=encoder, indent=indent)
            else:
                with open(self.output_file, 'w') as out_file:
                    json.dump([self.root], out_file, cls=encoder, indent=indent)
        except FileNotFoundError:
            self.logger.error("output file: %s could not be opened", self.output_file)
