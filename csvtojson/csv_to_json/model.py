'''
class for DTO for storing the input csv data in python object format
'''

__author__ = 'ayush.abhinav@tcs.com'

import json

class Item:
    '''
    class representing each node in the object tree
    '''
    def __init__(self, name, id, link):
        self.id = id
        self.label = name
        self.link = link
        self.children = []


    def add_child(self, child):
        '''
        add a child node to this node
        :param child: node to be added
        :return: None
        '''
        if isinstance(child, Item):
            self.children.append(child)
        else:
            raise TypeError(" {} is not of type item".format(child))


    def __eq__(self, other):
        if self.id != other.id:
            return False
        if self.label != other.label:
            return False
        if self.link != other.link:
            return False
        return True

    def __str__(self):
        return 'id :{0}, label : {1}, link :{2}'.format(self.id, self.label, self.link)






class ItemEncoder(json.JSONEncoder):
    '''
    Custom encoder for Item class
    '''

    def default(self, o):
        return o.__dict__
