'''
run defined tests
'''

__author__ = 'ayush.abhinav@tcs.com'

import unittest
from csv_to_json.test import test_csv_to_json, test_util, test_model


unittest.main(test_csv_to_json, exit=False)
unittest.main(test_util, exit=False)
unittest.main(test_model)
