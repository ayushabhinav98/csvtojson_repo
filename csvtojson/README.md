# Environment
Code has been tested to run on Linux, Unix, macOS

Python version > 3.5

# change to project folder
cd <path-to-csv_to_json>   # cd /Users/ayushabhinav/csv_to_json

# Running code in local linux environment
python main.py

# To get help 
python main.py --help

# To provide command_line argument
python main.py --input_file=<path_to_input_csv_file> --output_file=<path_to_output_json_file>

# To run program in debug mode
python main.py --debug

# To run test cases
python test.py


# Docker 
There is dockerfile in the project folder. It can be used to build docker image
To build docker image use following command

docker build -t <image_name> *

Eg: docker build -t ayush/csvtojson

#Kubernetes
There is also sample job.yaml file attached to run this as job in kubernetes.
It consider that the image is present in the registry to pull from. So image Pull policy is set to never.
To create job in kubernetes, use following sample command.

kubectl create -f <path_to_job.yaml>


# Code Algorithm
1. Get the generator to csv file. This will read the csv file one line at a time and return the line.

2. Tokenize the line

	2a. Convert the line into the list of words seperated by comma

	2b. Consider the word which has comma in them like "Pizza, Pasta & Sause" as one word "Pizza  Pasta & sause" instead of two word "Pizza" and "Pasta & Suase"

3. Creat the object tree from the tokens. In object tree parent object will contain the child object. Eg:- Dessert object will contain ice-cream object

4. Once the object tree in created, write them in Json file usng the custom Json encoder  











