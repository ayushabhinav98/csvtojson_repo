'''
main method for running from command line
'''

__author__ = 'ayush.abhinav@tcs.com'

import os
import sys
import argparse
import logging

from csv_to_json.csv_to_json import CSVToJSON


if __name__ == "__main__":
    root_folder = os.path.dirname(__file__)
    input_file = os.path.join(root_folder, "data.csv")
    out_file = os.path.join(root_folder, "out.json")

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", default=input_file,
                        help="input csv file. Default:{}".format(input_file))
    parser.add_argument("--output_file", default=sys.stdout,
                        help='output json file. Default: stdout')
    parser.add_argument("--debug", help="run program in debug mode", action="store_true")
    args = parser.parse_args()

    # Setting the logger.
    logger = logging.getLogger()
    if args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # Starting the conversion
    csv_to_json = CSVToJSON(args.input_file, args.output_file)
    root = csv_to_json.create_object()
    #csv_to_json.print_item()      # Method to print the object in tree format with indentation.
    csv_to_json.get_json()
